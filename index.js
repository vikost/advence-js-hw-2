'use strict'

//  Конструкцию `try...catch` уместно использовать когда мы получаем данные из вне,
// которые должны пройти проверку на коректность

const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];


  function createsList(arr) {
    const list = document.createElement('ul');
    arr.forEach((book, index) => {
    try{
      if (!book.author){
        throw new Error(`У книги №${index+1} нет автора`);
      }
      if (!book.name){
        throw new Error(`У книги №${index+1} нет названия`);
      }
      if (!book.price){
        throw new Error(`У книги №${index+1} нет цены`);
      }
      const listItem = document.createElement('li');
      listItem.innerHTML = `<i>${book.name}</i> ${book.author} - ${book.price}грн`;
      list.append(listItem);
    }catch (error) {
        console.error(error.message);
    }
    });
    return list
  }
  
  document.querySelector('#root').append(createsList(books));
  
// Первая функция впринципи выполняет ТЗ, но я написал более универсальную на всякий случай

// function createsList2(arr, ...props) {
//   const list = document.createElement('ul');
//   arr.forEach((book, index) => {
//   try{
//     props.forEach(prop =>{
//       if (!book[prop]){
//         throw new Error(`Book №${index+1} has not ${prop}`);
//       }
//     });
//     const listItem = document.createElement('li');
//     listItem.innerHTML = `<i>${book.name}</i> ${book.author} - ${book.price}$`;
//     list.append(listItem);
//   }catch (error) {
//       console.error(error.message);
//   }
//   });
//   return list
// }

// const booksList = createsList2(books, 
//   'author', 
//   'name', 
//   'price');

// document.querySelector('#root').append(booksList);